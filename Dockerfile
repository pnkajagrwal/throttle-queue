
# Dockerfile

FROM phusion/baseimage
MAINTAINER Pankaj Agrawal <pnkaj.agrwal@gmai.>

# Install Java.
RUN \
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  add-apt-repository -y ppa:webupd8team/java && \
  apt-get update && \
  apt-get install -y oracle-java8-installer && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/oracle-jdk8-installer

# Define commonly used JAVA_HOME variable
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

RUN rm -rf /etc/service/sshd /etc/my_init.d/00_regen_ssh_host_keys.sh

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /app

USER daemon

ADD ./target/throttle-queue-1.0.0-SNAPSHOT.jar /app/throttle-queue-1.0.0-SNAPSHOT.jar

CMD [ "java", "-jar", "/app/throttle-queue-1.0.0-SNAPSHOT.jar" ]

EXPOSE 8493
