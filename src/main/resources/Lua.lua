if redis.call("HEXISTS",KEYS[1],ARGV[1]) then
    local response = redis.call("HGET",KEYS[1],ARGV[1])
    local stringified = tostring(response)
    local intified = tonumber(stringified)
    if intified ~= nil then
        if intified < tonumber(ARGV[2]) then
            return redis.call("HSET",KEYS[1],ARGV[1],ARGV[2])
        else
            return "fail1"
        end
    else
        return "fail2:" .. stringified
    end
else
    return "fail3"
end