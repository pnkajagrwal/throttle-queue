package com.tfu.throttel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RedisConfig {

    private final String host;
    private final int port;

    public RedisConfig(@JsonProperty("host") String host,
                       @JsonProperty("port") int port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
