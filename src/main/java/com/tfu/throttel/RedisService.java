package com.tfu.throttel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.service.Service;
import ratpack.service.StopEvent;
import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;

@Singleton
public class RedisService implements Service {

    private static final Logger LOG = LoggerFactory.getLogger(RedisService.class);
    private final RedisConfig redisConfig;
    private RedisServer redisServer;

    @Inject
    public RedisService(RedisConfig redisConfig) throws IOException {
        this.redisConfig = redisConfig;
        redisServer = new RedisServer(redisConfig.getPort());
        redisServer.start();
        LOG.info("Started redis server successfully!");
    }

    public Jedis redis() throws IOException {
        return new Jedis(redisConfig.getHost(), redisConfig.getPort());
    }

    @Override
    public void onStop(StopEvent event) throws Exception {
        LOG.info("Stopping redis server!");
        redisServer.stop();
        LOG.info("Stopped redis server successfully!");
    }
}
