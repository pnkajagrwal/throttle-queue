package com.tfu.throttel.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
public class ThrottleResponse {
    @JsonProperty("Success")
    private Boolean success;
    @JsonProperty("Result")
    private Result result;

    public static ThrottleResponse failure() {
        return new ThrottleResponse(false);
    }

    public static ThrottleResponse success(Result result) {
        return new ThrottleResponse(result);
    }

    private ThrottleResponse(Boolean success) {
        this.success = success;
    }

    private ThrottleResponse(Result result) {
        success = true;
        this.result = result;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Result getResult() {
        return result;
    }
}
