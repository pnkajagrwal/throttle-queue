package com.tfu.throttel.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
public class Result {
    @JsonProperty("Availability")
    private Boolean availability;
    @JsonProperty("EstimatedWaitTime")
    private Double estimatedWaitTime;
    @JsonProperty("TotalRequestNoBefore")
    private Long totalRequestNoBefore;
    @JsonProperty("UtilizationRate")
    private Double utilizationRate;
    @JsonProperty("RequestID")
    private Long requestID;
    @JsonProperty("Message")
    private String message;

    public static Result available(Long totalRequestNoBefore,
                                   Double utilizationRate,
                                   Long requestID) {
        return new Result(true, null, totalRequestNoBefore, utilizationRate, requestID);
    }

    public static Result wait(Double estimatedWaitTime,
                              Long totalRequestNoBefore,
                              double utilizationRate,
                              Long requestID) {
        return new Result(false, estimatedWaitTime, totalRequestNoBefore, utilizationRate, requestID);
    }

    public static Result message(String message) {
        return new Result(message);
    }

    private Result(String message) {
        this.message = message;
    }

    private Result(Boolean availability,
                   Double estimatedWaitTime,
                   Long totalRequestNoBefore,
                   double utilizationRate,
                   Long requestID) {
        this.availability = availability;
        this.estimatedWaitTime = estimatedWaitTime;
        this.totalRequestNoBefore = totalRequestNoBefore;
        this.utilizationRate = utilizationRate;
        this.requestID = requestID;
    }

    public Boolean getAvailability() {
        return availability;
    }

    public Long getTotalRequestNoBefore() {
        return totalRequestNoBefore;
    }

    public Double getUtilizationRate() {
        return utilizationRate;
    }

    public Long getRequestID() {
        return requestID;
    }

    public Double getEstimatedWaitTime() {
        return estimatedWaitTime;
    }

    public String getMessage() {
        return message;
    }
}
