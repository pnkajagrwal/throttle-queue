package com.tfu.throttel.request;

import com.tfu.throttel.utilisation.UtilisationConfig;
import org.apache.commons.io.IOUtils;
import ratpack.form.Form;

import java.io.IOException;
import java.util.Base64;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

public class Request {
    private final Long requestId;
    private final Boolean forConfirmation;
    private final String destinationEndpoint;
    private final String authKey;
    private UtilisationConfig config;

    public Request(Form form) {
        this.requestId = isNotEmpty(form.get("request_id")) ? Long.valueOf(form.get("request_id")) : null;
        this.forConfirmation = isNotEmpty(form.get("for_confirmation")) ? Boolean.valueOf(form.get("for_confirmation")) : null;
        this.destinationEndpoint = getDestinationEndpoint(form);
        this.authKey = form.getOrDefault("auth_key", null);
    }

    private String getDestinationEndpoint(Form form) {
        try {
            return form.containsKey("destination_endpoint") ? IOUtils.toString(Base64.getDecoder().decode(form.get("destination_endpoint")), "UTF-8") : null;
        }
        catch (Exception e) {
            throw new ValidationError("Invalid destination endpoint");
        }
    }

    public Long getRequestId() {
        return requestId;
    }

    public Boolean getForConfirmation() {
        return forConfirmation;
    }

    public String getDestinationEndpoint() {
        return destinationEndpoint;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setConfig(UtilisationConfig config) {
        this.config = config;
    }

    public UtilisationConfig getConfig() {
        return config;
    }
}
