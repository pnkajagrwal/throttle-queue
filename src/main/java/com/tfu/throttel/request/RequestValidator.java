package com.tfu.throttel.request;

import com.tfu.throttel.ThrottleConfig;

import javax.inject.Inject;

import static java.util.Objects.isNull;
import static org.apache.commons.lang.StringUtils.isEmpty;

public class RequestValidator {
    private final ThrottleConfig throttleConfig;

    @Inject
    public RequestValidator(ThrottleConfig throttleConfig) {
        this.throttleConfig = throttleConfig;
    }

    public void validate(Request request) {
        if (isNull(request.getRequestId())) {
            throw new ValidationError("Request id not present.");
        }

        if (isNull(request.getForConfirmation())) {
            throw new ValidationError("For confirmation not present.");
        }

        if (isEmpty(request.getDestinationEndpoint())) {
            throw new ValidationError("Destination endpoint not present.");
        }

        if (notFromAllowedDomain(request.getDestinationEndpoint())) {
            throw new ValidationError("Destination endpoint is not from allowed domains.");
        }

        if (isEmpty(request.getAuthKey())) {
            throw new ValidationError("No Auth key present.");
        }
    }

    private boolean notFromAllowedDomain(String destinationEndpoint) {
        return throttleConfig.getAllowedDomains().stream()
                .noneMatch(destinationEndpoint::contains);
    }
}
