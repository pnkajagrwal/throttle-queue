package com.tfu.throttel.request;

public class ValidationError extends RuntimeException {

    public ValidationError(String message) {
        super(message);
    }
}
