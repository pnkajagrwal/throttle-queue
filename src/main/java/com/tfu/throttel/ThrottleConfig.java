package com.tfu.throttel;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.List;

import static java.util.Collections.unmodifiableList;

public class ThrottleConfig {
    private final List<String> allowedDomains;
    private final URI remoteConfigUri;
    private final URI timerServiceUri;
    private final long currentMaxParallelQueryNo;
    private final long currentMaxAllowedExecutionTimeMs;
    private final long currentAverageResponseTimeMs;
    private final int currentAPILoadFactor;

    public ThrottleConfig(@JsonProperty("allowedDomains") List<String> allowedDomains,
                          @JsonProperty("remoteConfigUri") URI remoteConfigUri,
                          @JsonProperty("timerServiceUri") URI timerServiceUri,
                          @JsonProperty("currentMaxParallelQueryNo") long currentMaxParallelQueryNo,
                          @JsonProperty("currentMaxAllowedExecutionTimeMs") long currentMaxAllowedExecutionTimeMs,
                          @JsonProperty("currentAverageResponseTimeMs") long currentAverageResponseTimeMs,
                          @JsonProperty("currentAPILoadFactor") int currentAPILoadFactor) {
        this.allowedDomains = allowedDomains;
        this.remoteConfigUri = remoteConfigUri;
        this.timerServiceUri = timerServiceUri;
        this.currentMaxParallelQueryNo = currentMaxParallelQueryNo;
        this.currentMaxAllowedExecutionTimeMs = currentMaxAllowedExecutionTimeMs;
        this.currentAverageResponseTimeMs = currentAverageResponseTimeMs;
        this.currentAPILoadFactor = currentAPILoadFactor;
    }

    public List<String> getAllowedDomains() {
        return unmodifiableList(allowedDomains);
    }

    public URI getRemoteConfigUri() {
        return remoteConfigUri;
    }

    public URI getTimerServiceUri() {
        return timerServiceUri;
    }

    public long getCurrentMaxParallelQueryNo() {
        return currentMaxParallelQueryNo;
    }

    public long getCurrentMaxAllowedExecutionTimeMs() {
        return currentMaxAllowedExecutionTimeMs;
    }

    public long getCurrentAverageResponseTimeMs() {
        return currentAverageResponseTimeMs;
    }

    public int getCurrentAPILoadFactor() {
        return currentAPILoadFactor;
    }
}
