package com.tfu.throttel;

import ratpack.guice.Guice;
import ratpack.server.BaseDir;
import ratpack.server.RatpackServer;

public class ThrottleQueue {

    public static void main(String args[]) throws Exception {
        RatpackServer.start(server -> server
                .serverConfig(serverConfigBuilder -> serverConfigBuilder
                        .baseDir(BaseDir.find())
                        .yaml("config/defaults.yaml")
                        .require("/application", AppConfig.class)
                        .require("/throttle", ThrottleConfig.class)
                        .require("/redis", RedisConfig.class))
                .registry(Guice.registry(bindings -> bindings
                        .bind(ThrottleHandler.class)
                        .bind(RedisService.class)))
                .handlers(chain -> chain
                        .get("", ThrottleHandler.class)));
    }
}
