package com.tfu.throttel;

import com.tfu.throttel.request.Request;
import com.tfu.throttel.request.RequestValidator;
import com.tfu.throttel.request.ValidationError;
import com.tfu.throttel.response.ThrottleResponse;
import com.tfu.throttel.utilisation.UtilisationCalculatorService;
import com.tfu.throttel.utilisation.UtilisationConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.form.Form;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.http.Status;

import javax.inject.Inject;

import static ratpack.jackson.Jackson.json;

public class ThrottleHandler implements Handler {
    private static final Logger LOG = LoggerFactory.getLogger(ThrottleHandler.class);

    private final RequestValidator requestValidator;
    private final UtilisationConfigService configService;
    private final UtilisationCalculatorService calculatorService;

    @Inject
    public ThrottleHandler(RequestValidator requestValidator,
                           UtilisationConfigService configService,
                           UtilisationCalculatorService calculatorService) {
        this.requestValidator = requestValidator;
        this.configService = configService;
        this.calculatorService = calculatorService;
    }

    @Override
    public void handle(Context ctx) throws Exception {
        ctx.parse(Form.form(true))
                .map(this::mapToRequest)
                .next(requestValidator::validate)
                .flatMap(configService::fetchOrDefault)
                .flatMap(calculatorService::calculate)
                .onError(ValidationError.class, validationError -> sendValidationErrorResponse(ctx, validationError))
                .then(result -> {
                    ctx.getResponse().status(Status.OK);
                    ctx.getResponse().contentType("application/json");
                    ctx.render(json(ThrottleResponse.success(result)));
                });
    }

    private void sendValidationErrorResponse(Context ctx,
                                             ValidationError error) {
        LOG.warn("Invalid request received. Reason: {}", error.getMessage());
        ctx.getResponse().status(Status.OK);
        ctx.getResponse().contentType("application/json");
        ctx.render(json(ThrottleResponse.failure()));
    }

    private Request mapToRequest(Form form) {
        return new Request(form);
    }
}
