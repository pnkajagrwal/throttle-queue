package com.tfu.throttel.utilisation;

public class CalcParameters {

    private long globalTimeStamp;
    private Long requestId;
    private int finishedOpenQueries;
    private Boolean query_1_done;
    private Boolean query_2_done;

    public CalcParameters() {
        this.finishedOpenQueries = 0;
        this.query_1_done = false;
        this.query_2_done = false;
    }

    public long getGlobalTimeStamp() {
        return globalTimeStamp;
    }

    public void setGlobalTimeStamp(long globalTimeStamp) {
        this.globalTimeStamp = globalTimeStamp;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public int incrementAndGetFinishedQueries() {
         finishedOpenQueries = ++finishedOpenQueries;
        return finishedOpenQueries;
    }

    public void setQuery_1_done(Boolean query_1_done) {
        this.query_1_done = query_1_done;
    }

    public void setQuery_2_done(Boolean query_2_done) {
        this.query_2_done = query_2_done;
    }

    public Boolean getQuery_1_done() {
        return query_1_done;
    }

    public Boolean getQuery_2_done() {
        return query_2_done;
    }
}
