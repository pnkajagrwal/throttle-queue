package com.tfu.throttel.utilisation;

import com.tfu.throttel.RedisService;
import com.tfu.throttel.request.Request;
import com.tfu.throttel.request.ValidationError;
import com.tfu.throttel.response.Result;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import ratpack.exec.Promise;
import redis.clients.jedis.Jedis;

import javax.inject.Inject;
import java.io.IOException;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.apache.commons.lang.BooleanUtils.toBoolean;
import static ratpack.exec.Promise.value;

public class UtilisationCalculatorService {

    private final Jedis redis;
    private final GlobalTimestampService timestampService;
    private String procedure;

    @Inject
    public UtilisationCalculatorService(RedisService redisService,
                                        GlobalTimestampService timestampService) throws IOException {
        this.redis = redisService.redis();
        this.procedure = IOUtils.toString(this.getClass().getResourceAsStream("/Lua.lua"), "UTF-8");
        this.timestampService = timestampService;
    }

    public Promise<Result> calculate(Request request) {
        if (toBoolean(request.getForConfirmation())) {
            return removeFromActiveRequests(request);
        }

        return calculateUtilisation(request);
    }

    private Promise<Result> removeFromActiveRequests(Request request) {
        if (request.getRequestId() < 0) {
            throw new ValidationError("Received request ID is incorrect in ForConfirmation. RequestID " + request.getRequestId());
        }

        Long response = redis.del("ActiveRequests:" + request.getRequestId());

        if (response == 1) {
            redis.hincrBy("SystemStatistics:activeRequestsStatistics", "statisticValue", -1);

        }

        return value(Result.message("Confirmed"));
    }

    private Promise<Result> calculateUtilisation(Request request) {
        return timestampService.fetchGlobalTImeStamp(request)
                .map(globalTimeStamp -> {
                    UtilisationConfig config = request.getConfig();
                    CalcParameters calc = new CalcParameters();
                    calc.setGlobalTimeStamp(globalTimeStamp);

                    Long requestId = request.getRequestId();
                    if (requestId < 0) {
                        requestId = redis.hincrBy("SystemStatistics:atomicRequestID", "statisticValue", 1);

                    }

                    calc.setRequestId(requestId);
                    return calculateUtilizationRate(request, config, calc);
                });
    }

    private Result calculateUtilizationRate(Request request,
                                            UtilisationConfig config,
                                            CalcParameters calcParameters) throws Exception {
        long lastRequestID;
        long lastEvaluatedRequestID;
        long sumExecutionTime;
        long howMany = 0;

        String activeRequestsStatistics = redis.hget("SystemStatistics:activeRequestsStatistics", "statisticValue");

        if (nonNull(activeRequestsStatistics)) {
            howMany = Math.max(Long.valueOf(activeRequestsStatistics), 0);
        }

        Map<String, String> requestQueueStatistics = redis.hgetAll("SystemStatistics:requestQueueStatistics");
        lastRequestID = Long.valueOf(requestQueueStatistics.getOrDefault("lastRequestID", "0"));
        lastEvaluatedRequestID = Long.valueOf(requestQueueStatistics.getOrDefault("lastEvaluatedRequestID", "0"));

        long calc = config.getCurrentAverageResponseTimeMs() * config.getCurrentMaxParallelQueryNo();

        if (calc == 0) {
            return step2(0, 0, calcParameters, request);
        }

        long lastVal = lastRequestID;

        if (calcParameters.getRequestId() > 0) {
            lastVal = Math.min(lastRequestID, request.getRequestId());
        }

        if (lastEvaluatedRequestID <= 0) {
            lastEvaluatedRequestID = lastRequestID;
        }

        howMany += Math.max(lastVal - lastEvaluatedRequestID, 0);
        sumExecutionTime = howMany * config.getCurrentAverageResponseTimeMs();

        double localUtilRate = 0;
        if (sumExecutionTime > 0) {
            localUtilRate = customRound((double) sumExecutionTime / calc / config.getCurrentAPILoadFactor(), 4);
        }

        return step2(localUtilRate, howMany, calcParameters, request);
    }

    private Result step2(double utilizationRate,
                         long totalRequestNoBefore,
                         CalcParameters calcParameters,
                         Request request) throws Exception {
        Double waitTime = 0D;
        Long requestId = calcParameters.getRequestId();
        UtilisationConfig config = request.getConfig();

        if (utilizationRate <= 1) {
            redis.del("RequestQueue:" + requestId);
            redis.eval(procedure, 1, "SystemStatistics:requestQueueStatistics", "lastEvaluatedRequestID", request.getRequestId().toString());
            calcParameters.setQuery_1_done(true);

            if (calcParameters.incrementAndGetFinishedQueries() >= 2) {
                buildResult(requestId, waitTime, totalRequestNoBefore, utilizationRate);
            }
        }
        else {
            waitTime = Math.ceil(customRound(config.getCurrentAverageResponseTimeMs() * utilizationRate, 2));
            Map<String, String> allDetails = redis.hgetAll("RequestQueue:" + requestId);

            Optional<Result> result;
            if (allDetails.isEmpty() || isNull(allDetails.get("lastServersideTimestamp"))) {
                result = notFoundInQueue(calcParameters, waitTime, request, utilizationRate, totalRequestNoBefore);
            }
            else {
                result = foundInQueue(calcParameters, waitTime, request, utilizationRate, totalRequestNoBefore);
            }

            if (result.isPresent()) {
                return result.get();
            }
        }

        if (waitTime <= 0) {
            String activeRequestKey = "ActiveRequests:" + requestId;
            String authKey = buildMd5String(request.getAuthKey());

            Map<String, String> value = new HashMap<>();
            value.put("lastServersideTimestamp", String.valueOf(calcParameters.getGlobalTimeStamp()));
            value.put("authKey", authKey);

            redis.hmset(activeRequestKey, value);
            redis.hincrBy("SystemStatistics:activeRequestsStatistics", "statisticValue", 1);
        }

        return query2Done(calcParameters, waitTime, totalRequestNoBefore, utilizationRate);
    }

    private Optional<Result> foundInQueue(CalcParameters calcParameters,
                                          double waitTime,
                                          Request request,
                                          double utilizationRate,
                                          long totalRequestNoBefore) throws IOException, NoSuchAlgorithmException {
        long requestId = calcParameters.getRequestId();
        String authKey = buildMd5String(request.getAuthKey());
        Map<String, String> value = new HashMap<>();

        value.put("lastServersideTimestamp", String.valueOf(calcParameters.getGlobalTimeStamp()));
        value.put("authKey", authKey);
        value.put("estimatedWaitTime", String.valueOf(waitTime));
        redis.hmset("RequestQueue:" + requestId, value);

        calcParameters.setQuery_1_done(true);

        if (calcParameters.incrementAndGetFinishedQueries() >= 2) {
            return of(buildResult(calcParameters.getRequestId(), waitTime, totalRequestNoBefore, utilizationRate));
        }

        return empty();
    }

    private Optional<Result> notFoundInQueue(CalcParameters calcParameters,
                                             double waitTime,
                                             Request request,
                                             double utilizationRate,
                                             long totalRequestNoBefore) throws IOException, NoSuchAlgorithmException {

        long requestId = calcParameters.getRequestId();
        String authKey = buildMd5String(request.getAuthKey());
        Map<String, String> value = new HashMap<>();

        value.put("lastServersideTimestamp", String.valueOf(calcParameters.getGlobalTimeStamp()));
        value.put("authKey", authKey);
        value.put("estimatedWaitTime", String.valueOf(waitTime));
        redis.hmset("RequestQueue:" + requestId, value);

        redis.eval(procedure, 1, "SystemStatistics:requestQueueStatistics", "lastEvaluatedRequestID", request.getRequestId().toString());
        calcParameters.setQuery_1_done(true);

        if (calcParameters.incrementAndGetFinishedQueries() >= 2) {
            return of(buildResult(requestId, waitTime, totalRequestNoBefore, utilizationRate));
        }

        return empty();
    }

    private Result query2Done(CalcParameters calcParameters,
                              Double waitTime,
                              long totalRequestNoBefore,
                              double utilizationRate) {
        calcParameters.setQuery_2_done(true);

        if (calcParameters.incrementAndGetFinishedQueries() >= 2) {
            return buildResult(calcParameters.getRequestId(), waitTime, totalRequestNoBefore, utilizationRate);
        }

        return buildResult(calcParameters.getRequestId(), waitTime, totalRequestNoBefore, utilizationRate);
    }


    public Result buildResult(long requestID,
                              Double waitTime,
                              Long totalRequestNoBefore,
                              double utilizationRate) {
        if (waitTime > 0) {
            return Result.wait(waitTime, totalRequestNoBefore, utilizationRate, requestID);
        }

        return Result.available(totalRequestNoBefore, utilizationRate, requestID);
    }

    private double customRound(double value,
                               int decimals) {
        String dec = StringUtils.leftPad("", decimals, "#");
        DecimalFormat df = new DecimalFormat("#." + dec);
        df.setRoundingMode(RoundingMode.CEILING);
        return Double.valueOf(df.format(value));
    }

    private String buildMd5String(String authKey) throws NoSuchAlgorithmException, IOException {
        byte[] bytesOfMessage = authKey.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("MD5");
        return IOUtils.toString(md.digest(bytesOfMessage), "UTF-8");
    }
}
