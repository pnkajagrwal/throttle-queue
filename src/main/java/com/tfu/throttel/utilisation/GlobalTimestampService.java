package com.tfu.throttel.utilisation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfu.throttel.ThrottleConfig;
import com.tfu.throttel.request.Request;
import com.tfu.throttel.request.ValidationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.exec.Promise;
import ratpack.http.client.HttpClient;

import javax.inject.Inject;

import static ratpack.exec.Promise.error;

public class GlobalTimestampService {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalTimestampService.class);

    private final HttpClient httpClient;
    private final ThrottleConfig throttleConfig;
    private final ObjectMapper objectMapper;

    @Inject
    public GlobalTimestampService(HttpClient httpClient,
                                  ThrottleConfig throttleConfig,
                                  ObjectMapper objectMapper) {
        this.httpClient = httpClient;
        this.throttleConfig = throttleConfig;
        this.objectMapper = objectMapper;
    }

    public Promise<Long> fetchGlobalTImeStamp(Request request) {
        return httpClient
                .get(throttleConfig.getTimerServiceUri(), spec -> spec.headers(mutableHeaders -> mutableHeaders.add("Content-Type", "application/json")))
                .map(receivedResponse -> objectMapper.readTree(receivedResponse.getBody().getBytes()).get("timestamp").asLong())
                .flatMapError(ex -> {
                    LOG.error("Failed fetching global time stamp for request id [{}]. Reason: {}", request.getRequestId(), ex.getMessage(), ex);
                    return error(new ValidationError("Could not get timestamp from timer service " + request.getRequestId()));
                });
    }
}
