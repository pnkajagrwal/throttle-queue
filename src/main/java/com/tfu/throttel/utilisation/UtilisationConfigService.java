package com.tfu.throttel.utilisation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfu.throttel.ThrottleConfig;
import com.tfu.throttel.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.exec.Promise;
import ratpack.http.client.HttpClient;
import ratpack.http.client.ReceivedResponse;

import javax.inject.Inject;
import java.io.IOException;
import java.time.Duration;

public class UtilisationConfigService {

    private static final Logger LOG = LoggerFactory.getLogger(UtilisationConfigService.class);

    private final ThrottleConfig throttleConfig;
    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;

    @Inject
    public UtilisationConfigService(ThrottleConfig throttleConfig,
                                    HttpClient httpClient,
                                    ObjectMapper objectMapper) {
        this.throttleConfig = throttleConfig;
        this.httpClient = httpClient;
        this.objectMapper = objectMapper;
    }

    public Promise<Request> fetchOrDefault(Request request) {
        return httpClient.get(throttleConfig.getRemoteConfigUri(),
                spec -> {
                    spec.connectTimeout(Duration.ofSeconds(1));
                    spec.readTimeout(Duration.ofSeconds(3));
                })
                .map(this::mapToConfig)
                .map(utilisationConfig -> {
                            request.setConfig(utilisationConfig);
                            return request;
                        }
                ).mapError(ex -> {
                    LOG.error("Failed fetching config remotely. Using default app config. Reason: {}", ex.getMessage(), ex);
                    request.setConfig(defaultConfig());
                    return request;
                });
    }

    private UtilisationConfig mapToConfig(ReceivedResponse receivedResponse) throws IOException {
        JsonNode jsonNode = objectMapper.readTree(receivedResponse.getBody().getBytes());

        return new UtilisationConfig(
                jsonNode.get("MaxParallelQueryNo").asLong(),
                jsonNode.get("MaxAllowedExecutionTimeMS").asLong(),
                jsonNode.get("AverageResponseTime").asLong(),
                jsonNode.get("APILoadFactor").asInt());
    }

    private UtilisationConfig defaultConfig() {
        return new UtilisationConfig(throttleConfig.getCurrentMaxParallelQueryNo(), throttleConfig.getCurrentMaxAllowedExecutionTimeMs(), throttleConfig.getCurrentAverageResponseTimeMs(), throttleConfig.getCurrentAPILoadFactor());
    }
}
