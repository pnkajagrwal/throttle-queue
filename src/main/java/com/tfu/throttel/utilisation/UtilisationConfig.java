package com.tfu.throttel.utilisation;

public class UtilisationConfig {
    private final long currentMaxParallelQueryNo;
    private final long currentMaxAllowedExecutionTimeMs;
    private final long currentAverageResponseTimeMs;
    private final int currentAPILoadFactor;

    public UtilisationConfig(long currentMaxParallelQueryNo,
                             long currentMaxAllowedExecutionTimeMs,
                             long currentAverageResponseTimeMs,
                             int currentAPILoadFactor) {
        this.currentMaxParallelQueryNo = currentMaxParallelQueryNo;
        this.currentMaxAllowedExecutionTimeMs = currentMaxAllowedExecutionTimeMs;
        this.currentAverageResponseTimeMs = currentAverageResponseTimeMs;
        this.currentAPILoadFactor = currentAPILoadFactor;
    }

    public long getCurrentMaxParallelQueryNo() {
        return currentMaxParallelQueryNo;
    }

    public long getCurrentMaxAllowedExecutionTimeMs() {
        return currentMaxAllowedExecutionTimeMs;
    }

    public long getCurrentAverageResponseTimeMs() {
        return currentAverageResponseTimeMs;
    }

    public int getCurrentAPILoadFactor() {
        return currentAPILoadFactor;
    }
}
