package com.tfu.throttel;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppConfig {

    private static final Logger LOG = LoggerFactory.getLogger(AppConfig.class);

    @JsonCreator
    public AppConfig(@JsonProperty("appLogLevel") final String appLogLevel) {
        setAppLogLevel(appLogLevel);
    }

    private void setAppLogLevel(String appLogLevel) {
        LOG.info("Setting app log level to: {}", appLogLevel);
        LogManager.getLogger("com.tufu.throttel").setLevel(Level.toLevel(appLogLevel));
    }
}
