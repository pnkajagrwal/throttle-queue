package com.tfu.throttel.utilisation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfu.throttel.ThrottleConfig;
import com.tfu.throttel.request.Request;
import com.tfu.throttel.request.ValidationError;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ratpack.exec.ExecResult;
import ratpack.exec.Promise;
import ratpack.http.client.HttpClient;
import ratpack.http.client.ReceivedResponse;
import ratpack.http.internal.ByteBufBackedTypedData;
import ratpack.test.exec.ExecHarness;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GlobalTimestampServiceTest {

    private GlobalTimestampService timestampService;
    @Mock
    private HttpClient httpClient;
    @Mock
    private ThrottleConfig throttleConfig;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        timestampService = new GlobalTimestampService(httpClient, throttleConfig, new ObjectMapper());
    }

    @Test
    public void should_fetch_global_time_stamp() throws Exception {
        byte[] bytes = IOUtils.toByteArray(this.getClass().getResourceAsStream("/external/timestamp.json"));
        ReceivedResponse receivedResponse = mock(ReceivedResponse.class);
        ByteBufBackedTypedData backedTypedData = mock(ByteBufBackedTypedData.class);
        when(backedTypedData.getBytes()).thenReturn(bytes);
        when(receivedResponse.getBody()).thenReturn(backedTypedData);

        when(httpClient.get(any(URI.class), any())).thenReturn(Promise.value(receivedResponse));
        ExecResult<Long> execResult = ExecHarness.yieldSingle(e -> timestampService.fetchGlobalTImeStamp(mock(Request.class)));

        assertThat(execResult.getValue()).isEqualTo(1500761983471L);
    }

    @Test
    public void should_give_validation_error_when_failed_fetching_global_timestamp() throws Exception {
        when(httpClient.get(any(URI.class), any())).thenReturn(Promise.error(new RuntimeException()));
        ExecResult<Long> execResult = ExecHarness.yieldSingle(e -> timestampService.fetchGlobalTImeStamp(mock(Request.class)));

        assertThat(execResult.getThrowable()).isInstanceOf(ValidationError.class);
    }
}