package com.tfu.throttel.utilisation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfu.throttel.ThrottleConfig;
import com.tfu.throttel.request.Request;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import ratpack.exec.ExecResult;
import ratpack.exec.Promise;
import ratpack.http.client.HttpClient;
import ratpack.http.client.ReceivedResponse;
import ratpack.http.internal.ByteBufBackedTypedData;
import ratpack.test.exec.ExecHarness;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class UtilisationConfigServiceTest {

    private UtilisationConfigService configService;
    @Mock
    private ThrottleConfig config;
    @Mock
    private HttpClient httpClient;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        when(config.getCurrentMaxParallelQueryNo()).thenReturn(1000L);
        when(config.getCurrentMaxAllowedExecutionTimeMs()).thenReturn(1000L);
        when(config.getCurrentAverageResponseTimeMs()).thenReturn(2500L);
        when(config.getCurrentAPILoadFactor()).thenReturn(2);
        configService = new UtilisationConfigService(config, httpClient, new ObjectMapper());
    }

    @Test
    public void should_provide_default_app_config_when_failed_fetching_config_remotely() throws Exception {
        Request request = mock(Request.class);
        when(httpClient.get(any(URI.class), any())).thenReturn(Promise.error(new RuntimeException()));
        ExecHarness.yieldSingle(e -> configService.fetchOrDefault(request));
        ArgumentCaptor<UtilisationConfig> argumentCaptor = ArgumentCaptor.forClass(UtilisationConfig.class);

        verify(request).setConfig(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue())
                .isEqualToComparingFieldByField(new UtilisationConfig(1000, 1000, 2500, 2));
    }

    @Test
    public void should_provide_config_when_fetched_config_remotely() throws Exception {
        Request request = mock(Request.class);
        byte[] bytes = IOUtils.toByteArray(this.getClass().getResourceAsStream("/external/config.json"));
        ReceivedResponse receivedResponse = mock(ReceivedResponse.class);
        ByteBufBackedTypedData backedTypedData = mock(ByteBufBackedTypedData.class);
        when(backedTypedData.getBytes()).thenReturn(bytes);
        when(receivedResponse.getBody()).thenReturn(backedTypedData);

        when(httpClient.get(any(URI.class), any())).thenReturn(Promise.value(receivedResponse));

        ExecHarness.yieldSingle(e -> configService.fetchOrDefault(request));

        ArgumentCaptor<UtilisationConfig> argumentCaptor = ArgumentCaptor.forClass(UtilisationConfig.class);

        verify(request).setConfig(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue())
                .isEqualToComparingFieldByField(new UtilisationConfig(500, 500, 1500, 1));
    }
}