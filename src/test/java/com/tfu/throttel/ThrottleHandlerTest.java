package com.tfu.throttel;

import com.google.common.collect.ImmutableMultimap;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ratpack.func.Action;
import ratpack.test.MainClassApplicationUnderTest;

import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

public class ThrottleHandlerTest {

    private MainClassApplicationUnderTest app;

    @Before
    public void setUp() throws Exception {
        app = new MainClassApplicationUnderTest(ThrottleQueue.class);
    }

    @After
    public void after() {
        app.close();
    }

    @Test
    public void should_fail_for_invalid_request_without_any_query_param() throws Exception {
        app.test(client -> {

            String response = client
                    .getText("");

            assertEquals(IOUtils.toString(this.getClass().getResourceAsStream("/response/failure.json"), "UTF-8"), response, true);
        });
    }

    @Test
    public void should_fetch_stats_for_valid_request() throws Exception {
        app.test(client -> {

            String response = client.params(validRequestParams(-1, false))
                    .getText("");

            assertEquals(IOUtils.toString(this.getClass().getResourceAsStream("/response/success.json"), "UTF-8"), response, true);
        });
    }

    @Test
    public void should_get_fail_response_for_invalid_request_id_for_confirmation() throws Exception {
        app.test(client -> {

            String response = client.params(validRequestParams(-1, true))
                    .getText("");

            assertEquals(IOUtils.toString(this.getClass().getResourceAsStream("/response/failure.json"), "UTF-8"), response, true);
        });
    }

    @Test
    public void should_get_valid_response_for_valid_request_for_confirmation() throws Exception {
        app.test(client -> {

            String response = client.params(validRequestParams(1, true))
                    .getText("");

            assertEquals(IOUtils.toString(this.getClass().getResourceAsStream("/response/confirmationSuccess.json"), "UTF-8"), response, true);
        });
    }

    private Action<ImmutableMultimap.Builder<String, Object>> validRequestParams(int requestId, boolean forConfirmation) {
        return stringObjectBuilder -> {
            stringObjectBuilder.put("request_id", requestId);
            stringObjectBuilder.put("for_confirmation", forConfirmation);
            stringObjectBuilder.put("destination_endpoint", "aHR0cHM6Ly9leGFtcGxlLWFwaS50ZnV0ZWFtLmNvbS9kZXYvdjEvZXhhbXBsZS5waHA=");
            stringObjectBuilder.put("auth_key", "MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY==");
        };
    }
}