package com.tfu.throttel.request;

import com.tfu.throttel.ThrottleConfig;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ratpack.form.Form;
import ratpack.form.internal.DefaultForm;
import ratpack.util.internal.ImmutableDelegatingMultiValueMap;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static ratpack.util.MultiValueMap.empty;

public class RequestValidatorTest {

    private RequestValidator requestValidator;
    @Mock
    private ThrottleConfig config;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        when(config.getAllowedDomains()).thenReturn(singletonList("tfuteam.com"));
        requestValidator = new RequestValidator(config);
    }

    @Test
    public void should_not_allow_request_from_not_supported_domain() throws Exception {
        Map<String, List<String>> queryParams = new HashMap<>();
        String notSupportedDomainUrl = IOUtils.toString(Base64.getEncoder().encode("http://www.google.com".getBytes()), "UTF-8");

        queryParams.put("request_id", singletonList("-1"));
        queryParams.put("for_confirmation", singletonList("false"));
        queryParams.put("destination_endpoint", singletonList(notSupportedDomainUrl));
        queryParams.put("auth_key", singletonList("MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY="));
        Form form = new DefaultForm(new ImmutableDelegatingMultiValueMap<>(queryParams), empty());

        assertThatExceptionOfType(ValidationError.class)
                .isThrownBy(() -> requestValidator.validate(new Request(form)))
                .withMessage("Destination endpoint is not from allowed domains.");
    }

    @Test
    public void should_not_allow_request_without_request_id() throws Exception {
        Map<String, List<String>> queryParams = new HashMap<>();

        queryParams.put("request_id", singletonList(EMPTY));
        queryParams.put("for_confirmation", singletonList("false"));
        queryParams.put("destination_endpoint", singletonList("aHR0cHM6Ly9leGFtcGxlLWFwaS50ZnV0ZWFtLmNvbS9kZXYvdjEvZXhhbXBsZS5waHA="));
        queryParams.put("auth_key", singletonList("MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY="));
        Form form = new DefaultForm(new ImmutableDelegatingMultiValueMap<>(queryParams), empty());

        assertThatExceptionOfType(ValidationError.class)
                .isThrownBy(() -> requestValidator.validate(new Request(form)))
                .withMessage("Request id not present.");
    }

    @Test
    public void should_not_allow_request_without_for_confirmation() throws Exception {
        Map<String, List<String>> queryParams = new HashMap<>();

        queryParams.put("request_id", singletonList("-1"));
        queryParams.put("for_confirmation", singletonList(EMPTY));
        queryParams.put("destination_endpoint", singletonList("aHR0cHM6Ly9leGFtcGxlLWFwaS50ZnV0ZWFtLmNvbS9kZXYvdjEvZXhhbXBsZS5waHA="));
        queryParams.put("auth_key", singletonList("MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY="));
        Form form = new DefaultForm(new ImmutableDelegatingMultiValueMap<>(queryParams), empty());

        assertThatExceptionOfType(ValidationError.class)
                .isThrownBy(() -> requestValidator.validate(new Request(form)))
                .withMessage("For confirmation not present.");
    }

    @Test
    public void should_not_allow_request_without_destination_endpoint() throws Exception {
        Map<String, List<String>> queryParams = new HashMap<>();

        queryParams.put("request_id", singletonList("-1"));
        queryParams.put("for_confirmation", singletonList("false"));
        queryParams.put("auth_key", singletonList("MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY="));
        Form form = new DefaultForm(new ImmutableDelegatingMultiValueMap<>(queryParams), empty());

        assertThatExceptionOfType(ValidationError.class)
                .isThrownBy(() -> requestValidator.validate(new Request(form)))
                .withMessage("Destination endpoint not present.");
    }

    @Test
    public void should_not_allow_request_without_auth_key() throws Exception {
        Map<String, List<String>> queryParams = new HashMap<>();

        queryParams.put("request_id", singletonList("-1"));
        queryParams.put("for_confirmation", singletonList("false"));
        queryParams.put("destination_endpoint", singletonList("aHR0cHM6Ly9leGFtcGxlLWFwaS50ZnV0ZWFtLmNvbS9kZXYvdjEvZXhhbXBsZS5waHA="));
        queryParams.put("auth_key", singletonList(EMPTY));
        Form form = new DefaultForm(new ImmutableDelegatingMultiValueMap<>(queryParams), empty());

        assertThatExceptionOfType(ValidationError.class)
                .isThrownBy(() -> requestValidator.validate(new Request(form)))
                .withMessage("No Auth key present.");
    }
}